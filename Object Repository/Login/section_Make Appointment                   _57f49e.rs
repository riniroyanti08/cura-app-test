<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_Make Appointment                   _57f49e</name>
   <tag></tag>
   <elementGuidId>8c267c8d-ff90-479f-bc68-4049f889fb4a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//section[@id='appointment']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#appointment</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>#appointment</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>section</value>
      <webElementGuid>9826b9cc-2759-4a17-9ba0-3af9f1c152b1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>appointment</value>
      <webElementGuid>57cd4207-3cb2-4afd-81db-ef49479e46be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>section bg-primary</value>
      <webElementGuid>300f18a4-4c47-468a-81e8-4c94224f2eef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
            
                Make Appointment
                
            
            
                
                    Facility
                    
                        
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        
                    
                
                
                    
                        
                             Apply for hospital readmission
                        
                    
                
                
                    Healthcare Program
                    
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    
                
                
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                
                
                    Comment
                    
                        
                    
                
                
                    
                        Book Appointment
                    
                
            
        
    
</value>
      <webElementGuid>8d8e5244-53c3-4b0d-aee2-111ed64317ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;appointment&quot;)</value>
      <webElementGuid>a6703aa8-42ea-4dd3-acdd-f74b145b67d8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//section[@id='appointment']</value>
      <webElementGuid>7c472cda-2bcd-47d8-877d-63707ba67664</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Make Appointment'])[1]/following::section[1]</value>
      <webElementGuid>c0377044-4568-45e7-b3a9-195b1309c9ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='We Care About Your Health'])[1]/following::section[1]</value>
      <webElementGuid>a51141b4-2435-42d9-8a19-ec909c418a8a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section</value>
      <webElementGuid>9eb4a7f6-b287-4b84-aac0-3da55ea9abdd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//section[@id = 'appointment' and (text() = '
    
        
            
                Make Appointment
                
            
            
                
                    Facility
                    
                        
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        
                    
                
                
                    
                        
                             Apply for hospital readmission
                        
                    
                
                
                    Healthcare Program
                    
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    
                
                
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                
                
                    Comment
                    
                        
                    
                
                
                    
                        Book Appointment
                    
                
            
        
    
' or . = '
    
        
            
                Make Appointment
                
            
            
                
                    Facility
                    
                        
                            Tokyo CURA Healthcare Center
                            Hongkong CURA Healthcare Center
                            Seoul CURA Healthcare Center
                        
                    
                
                
                    
                        
                             Apply for hospital readmission
                        
                    
                
                
                    Healthcare Program
                    
                        
                             Medicare
                        
                        
                             Medicaid
                        
                        
                             None
                        
                    
                
                
                    Visit Date (Required)
                    
                        
                            
                            
                                
                            
                        
                    
                
                
                    Comment
                    
                        
                    
                
                
                    
                        Book Appointment
                    
                
            
        
    
')]</value>
      <webElementGuid>0f5feff1-387e-4ab4-b24c-2901e5142612</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
