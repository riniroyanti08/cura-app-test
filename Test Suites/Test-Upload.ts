<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test-Upload</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>a10936d6-3566-4235-931c-bf57d4e1d210</testSuiteGuid>
   <testCaseLink>
      <guid>67c9b83b-d15c-4362-8087-a9b5b1b0aac3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Upload File/Upload003 - Upload file test driven</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>f2a5a200-2f88-4e22-8cd8-5681dec9476c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Test-Data-Upload</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>f2a5a200-2f88-4e22-8cd8-5681dec9476c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>URLDIR</value>
         <variableId>05bd3ef8-276d-41c8-b12f-0c76be7bc149</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
